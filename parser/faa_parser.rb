require 'csv'


module Parser
  class FaaParser

    FaaItem = Struct.new(:locationId, :name, :phone)

    def parse (fileName)

      @allFaaData = []
      CSV.foreach(fileName, {:col_sep => "\t"}) do |row|
        phone = String(row[17])
        @allFaaData << FaaItem.new(String(row[2].tr("'","")).strip, String(row[11]).strip, String(row[17]).strip)
      end
      @allFaaData
    end
  end
end
