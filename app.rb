require 'sinatra'
require 'json'
require './parser/faa_parser'


parser = Parser::FaaParser.new

faaData = parser.parse("./NfdcFacilities.csv")

get '/' do
  'Hello World'
end

get "/faa/:name" do
  content_type :json
  input_id = params['name']
  foundItem=nil
  for i in 0 ... faaData.size
    if faaData[i].locationId == input_id
        foundItem = faaData[i]
      break
    end
  end
  foundItem.to_h.to_json
end